# Landslide Demo

There are several Landslide demos in Velocity. All of them relies on the same easy topology, and some testcases, each one focused on different test type.

For those demos, Velocity provides:

- Topology
- Test cases
- Runlist
- Reports
- Kibana Dahsboard


# Topology

![enter image description here](Standard_LS_Demo.png)

It uses TS76 and TS77 on eth4.

The topology can be used from iTest and has a LS session attached to the **Nodal** resource.

# Available test cases

Test cases are available in the repository **TS_Demo** inside folder **ai_LS_Demo/test_cases**:

All those test cases needs the topology **StandardDemo/LS/Standard_LS_Demo** to be selected when executed, or alternatively, they can be executed manually from the topology editor with that same topology reserved.

## Kibana Index reset

There are three test cases that publish data into Elastic and it can be seen in the Kibana Dashboard (Elastic + Kibana = ELK). ELK indexes need to be created before data can be published. Those test cases create those indexes or restore (deleting all existing data) if they already exist.

**Those shouldn't be executed unless there are problems with ELK dashboards or corrupted data.**

The available index creation test cases are:

- **CreateIndexIuCS.fftc**: It creates/restores IuCS related ELK index. 
- **CreateIndexVoLTE.fftc**: It creates/restores VoLTE related ELK index. 
- **CreateIndexVoWIFI.fftc**: It creates/restores VoWIFI related ELK index. 

## IuCS

**LS:**

_UE connected to 3G Iu-CS using the sequencer in MME nodal test case. The UE establishes a call with an endpoint for 20 seconds. Packets sent/sec and Packets rcvd/sec as pass/fail criteria. The tets session runs for 300 secs._

It runs a IuCS LS test with a pass/fail criteria. Test rill run until LS test finish or timeout is reached.

It has some parameters defined. They have default values, but their value can be changed from Velocity if that is needed:

- **ELK_IP**: ELK server IP.
- **ELK_index**: Index where to publish data.
- **test**: Name of the test execution. Date and time will be append to this string to allow different names for different executions and be able to compare results in ELK.
- **RTPmin**: Pass criteria. Min RTP pps to be accepted as pass.
- **maxTime**: Max execution time. This value is the one expected based in the used LS test configuration.
- **extraTime**: Time added to "maxTime" to calculate test timeout. If total execution time is greater than maxTime+extraTime, test will abort and fail.

## VoLTE

**LS:**

_10000 UEs EPS attached and IMS registered establish a VoLTE call with sip endpoints.
Random POLQA measurement of 10 channels.
Pass/fail criteria for MOS  < 3.5.
Calls last 30 seconds.
Test session running for 3 minutes._


It runs a VoLTE LS test with a pass/fail criteria. Test rill run until LS test finish or timeout is reached.

It has some parameters defined. They have default values, but their value can be changed from Velocity if that is needed:

- **ELK_IP**: ELK server IP.
- **ELK_index**: Index where to publish data.
- **test**: Name of the test execution. Date and time will be append to this string to allow different names for different executions and be able to compare results in ELK.
- **minMOS**: Pass criteria. Min MOS value to be accepted as pass.
- **maxTime**: Max execution time. This value is the one expected based in the used LS test configuration.
- **extraTime**: Time added to "maxTime" to calculate test timeout. If total execution time is greater than maxTime+extraTime, test will abort and fail.

## VoWIFI

**LS:**

_One Wi-Fi UE (+ AP emulation) connected to the WOGW and establishes a VoIP call with a SIP Endpoint for 20 seconds. POLQA measurement of this call and pass/fail criteria for MOS >3.0. The test session runs for 60 seconds_

It runs a VoWIFI LS test with a pass/fail criteria. Test rill run until LS test finish or timeout is reached.

It has some parameters defined. They have default values, but their value can be changed from Velocity if that is needed:

- **ELK_IP**: ELK server IP.
- **ELK_index**: Index where to publish data.
- **test**: Name of the test execution. Date and time will be append to this string to allow different names for different executions and be able to compare results in ELK.
- **maxTime**: Max execution time. This value is the one expected based in the used LS test configuration.
- **extraTime**: Time added to "maxTime" to calculate test timeout. If total execution time is greater than maxTime+extraTime, test will abort and fail.

Pass/Fail criteria is set inside LS test case, and Velocity will use it to show it as Pass/Fail

## CS-SMS_MO 

**LS:**

_2000 UEs send control plane SMS every 20 seconds. Pass/Fail criteria on SMS data send = SMS data received. Test session runs for 70 seconds._

It runs a CS-SMS_MO LS test with a pass/fail criteria. Test rill run until LS test finish or timeout is reached.

It has some parameters defined. They have default values, but their value can be changed from Velocity if that is needed:

- **ELK_IP**: ELK server IP.
- **ELK_index**: Index where to publish data.
- **test**: Name of the test execution. Date and time will be append to this string to allow different names for different executions and be able to compare results in ELK.
- **maxTime**: Max execution time. This value is the one expected based in the used LS test configuration.
- **extraTime**: Time added to "maxTime" to calculate test timeout. If total execution time is greater than maxTime+extraTime, test will abort and fail.

Pass/Fail criteria is set inside LS test case, and Velocity will use it to show it as Pass/Fail

## CS-SMS_MT 

**LS:**

_2000 UEs receive control plane SMS every 10 seconds. Pass/Fail criteria on SMS data send = SMS data received. Test session runs for 70 seconds._

It runs a CS-SMS_MT LS test with a pass/fail criteria. Test rill run until LS test finish or timeout is reached.

It has some parameters defined. They have default values, but their value can be changed from Velocity if that is needed:

- **ELK_IP**: ELK server IP.
- **ELK_index**: Index where to publish data.
- **test**: Name of the test execution. Date and time will be append to this string to allow different names for different executions and be able to compare results in ELK.
- **maxTime**: Max execution time. This value is the one expected based in the used LS test configuration.
- **extraTime**: Time added to "maxTime" to calculate test timeout. If total execution time is greater than maxTime+extraTime, test will abort and fail.

Pass/Fail criteria is set inside LS test case, and Velocity will use it to show it as Pass/Fail

## IP-SMS_MO 

**LS:**

_2000 UEs send SMS over IP every 20 seconds. Pass/Fail criteria on SMS data send = SMS data received. Test session runs for 100 seconds._

It runs a IP-SMS_MO LS test with a pass/fail criteria. Test rill run until LS test finish or timeout is reached.

It has some parameters defined. They have default values, but their value can be changed from Velocity if that is needed:

- **ELK_IP**: ELK server IP.
- **ELK_index**: Index where to publish data.
- **test**: Name of the test execution. Date and time will be append to this string to allow different names for different executions and be able to compare results in ELK.
- **maxTime**: Max execution time. This value is the one expected based in the used LS test configuration.
- **extraTime**: Time added to "maxTime" to calculate test timeout. If total execution time is greater than maxTime+extraTime, test will abort and fail.

Pass/Fail criteria is set inside LS test case, and Velocity will use it to show it as Pass/Fail


## IP-SMS_MT 

**LS:**

_2000 UEs receive SMS over IP every 20 seconds. Pass/Fail criteria on SMS data send = SMS data received. Test session runs for 100 seconds._

It runs a CS-SMS_MO LS test with a pass/fail criteria. Test rill run until LS test finish or timeout is reached.

It has some parameters defined. They have default values, but their value can be changed from Velocity if that is needed:

- **ELK_IP**: ELK server IP.
- **ELK_index**: Index where to publish data.
- **test**: Name of the test execution. Date and time will be append to this string to allow different names for different executions and be able to compare results in ELK.
- **maxTime**: Max execution time. This value is the one expected based in the used LS test configuration.
- **extraTime**: Time added to "maxTime" to calculate test timeout. If total execution time is greater than maxTime+extraTime, test will abort and fail.

Pass/Fail criteria is set inside LS test case, and Velocity will use it to show it as Pass/Fail

# Runlist

There is a Runlist define in Velocity (**Standard_LS_Demo.vrl**)that includes all the test cases (except the ELK index restore ones) that allows execute all the test in one step. 

Run list has associated the required topology, so just execute it will reserve the topology and execute the seven test cases one after the other.

# Kibana Dashboard

Logging into Kibana (https://10.75.236.3/kibana) allows to show Kibana dashboards built for this Demo.

There are three graphs shown, related to the Pass/Fail KPI for IuCS, VoLTE and VoWIFI

Meanwhile the indexes aren't restored with the test cases shown above, all the historical data will be available.

Graphs are updated while the test is running, but the user need to refresh the browser to show them.

![enter image description here](Standard_LS_Dashboard.png)

Be aware that the period of time to be shown can be selected in the top right. By default it shows the last 15 minutes, and most of the times it needs to be changed to the apropiarte time.

