# Service Based Demo

There are several Landslide demos in Velocity. All of them relies on the same easy topology, and some testcases, each one focused on different test type.

For those demos, Velocity provides:

- Topology
- Test cases
- Runlist
- Reports
- Kibana Dahsboard


# Topology

![enter image description here](Service_Based.png)

It uses TS1 on eth0, which emulates: NRF, Ausf, UDM Nodes


# Available test cases

Test Cases available:

- **Service Based NRF Registration.fftc**. This test tries to register 2 sessions against the NRF and checks if the result is successful.

![enter image description here](registrations.png)
